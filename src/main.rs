use ncurses::*;
use rustfft::{num_complex::Complex, FftPlanner};
use std::io::{self, Read};
use std::thread;
use std::time::{Duration, Instant};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    // Initialize ncurses
    initscr();
    cbreak();
    noecho();
    nodelay(stdscr(), true);
    keypad(stdscr(), true);
    curs_set(CURSOR_VISIBILITY::CURSOR_INVISIBLE);

    // Set up audio input parameters
    let sample_rate: u32 = 48000; // Set the sample rate of your audio
    let channels: u16 = 2; // Set the number of audio channels
    let bits_per_sample: u16 = 16; // Set the bits per sample

    // set frame-rate from command line argument and default to 60
    let frame_rate = std::env::args()
        .nth(1)
        .and_then(|arg| arg.parse().ok())
        .unwrap_or(60.0);
    let mut gain = std::env::args()
        .nth(2)
        .and_then(|arg| arg.parse().ok())
        .unwrap_or(10.0);
    // Calculate the frames per buffer based on the desired frame rate
    let frames_per_buffer = (sample_rate as f32 / frame_rate).round() as usize;
    let mut max_magnitudes_last_10_seconds = vec![0.0; 600];

    // Prepare the buffer for reading audio data from stdin
    let bytes_per_frame = (bits_per_sample / 8) as usize * channels as usize;
    let mut buffer = vec![0u8; frames_per_buffer * bytes_per_frame];
    let fft_size = buffer.len();

    let mut input: Vec<Complex<f32>> = vec![Complex::new(0.0, 0.0); fft_size];

    // Calculate the frame duration
    let frame_duration = Duration::from_secs_f32(1.0 / frame_rate);

    let mut planner = FftPlanner::new();
    // Create an FFT planner
    let sample_size = (bits_per_sample / 8) as usize;
    // Determine the size of the FFT
    let num_samples = fft_size / sample_size;
    let fft = planner.plan_fft_forward(fft_size);
    // get lines and columns
    let mut lines = 0;
    let mut cols = 0;

    loop {
        let start_time = Instant::now();

        // Read audio data from stdin
        if let Ok(_) = io::stdin().read_exact(&mut buffer) {
            getmaxyx(stdscr(), &mut lines, &mut cols);
            // Prepare input and output buffer for the FFT
            // Convert the audio samples to complex representation
            // average the values for each sample while retaining channel separation
            for i in 0..num_samples {
                let start = i * sample_size;
                let end = start + sample_size;
                let mut left_sample = 0.0;
                let mut right_sample = 0.0;
                for j in start..end {
                    let sample = buffer[j] as f32;
                    if j % 2 == 0 {
                        left_sample += sample;
                    } else {
                        right_sample += sample;
                    }
                }
                input[i] = Complex::new(left_sample / (sample_size / 2) as f32, 0.0);
                input[i + num_samples] = Complex::new(right_sample / (sample_size / 2) as f32, 0.0);
            }

            // Perform the FFT
            fft.process(&mut input);

            // Calculate the magnitudes of the complex spectrum
            let magnitudes: Vec<f32> = input.iter().map(|c| c.norm()).collect();

            let mut max_magnitude = magnitudes.iter().fold(0.0, |max, &mag| mag.max(max));
            max_magnitudes_last_10_seconds.push(max_magnitude);
            max_magnitudes_last_10_seconds.remove(0);
            max_magnitude = max_magnitudes_last_10_seconds.iter().fold(0.0, |max, &mag| mag.max(max));
            let spectrum: Vec<f32> = magnitudes.iter().map(|&mag| mag * gain / max_magnitude).collect();

            // shorten spectrum length to fit the screen by spreading the values out over the 2 times the colums of the screen.
            let spectrum: Vec<f32> = spectrum
                .chunks(spectrum.len() / (cols as usize * 2))
                .map(|chunk| chunk.iter().sum::<f32>() / chunk.len() as f32)
                .collect(); 

            // Display spectrum analysis
            for (i, &amplitude) in spectrum.iter().enumerate() {
                // calculate the bar height based on the amplitude centered from the middle of the screen
                let bar_height = amplitude * (lines as f32 / 2.0);
                // draw the bar graph for each channel
                for channel in 0..2 {
                    // draw the bar graph for each channel
                   for j in 0..lines {                        // Draw the bar graph
                        let bar_char = if j < bar_height as i32 {
                            ACS_CKBOARD() 
                        } else if j == bar_height as i32 {
                            // smooth top bar graph with every ncurse character of blocks
                            let remainder = bar_height - bar_height.floor();
                            if remainder > 0.0 && remainder < 0.25 {
                                ACS_ULCORNER()
                            } else if remainder > 0.25 && remainder < 0.5 {
                                ACS_DIAMOND()
                            } else if remainder > 0.5 && remainder < 0.75 {
                                ACS_CKBOARD()
                            } else {
                                ACS_BLOCK()
                            }
                            
                        } else {
                            ' ' as chtype
                        };
                        // draw the bar graph for each channel
                        if channel == 0 {
                            mvaddch((lines / 2) - j, i as i32, bar_char);
                        } else {
                            mvaddch((lines / 2) + j, i as i32, bar_char);
                        }
                    }
                }

                
               
            }
            
            // Refresh the screen
            refresh();
        }
        // Handle keypresses using getch with type i32 + and - to adjust gain
        match getch() {
            43 => gain += 1.0,
            45 => gain -= 1.0,
            _ => (),
        }
        

        // Calculate the elapsed time for the frame
        let elapsed_time = start_time.elapsed();

        // Wait until it's time to draw the next frame
        if frame_duration > elapsed_time {
            thread::sleep(frame_duration - elapsed_time);
        }
    }
}
